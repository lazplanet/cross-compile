# Cross Compiling Example on Lazarus

If you want binaries, visit the [Releases](https://gitlab.com/lazplanet/cross-compile/-/releases) section.

Article and video tutorial: https://lazplanet.blogspot.com/2020/05/cross-compile-on-lazarus.html
